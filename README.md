# Evaluation of CD-HIT for constructing non-redundant databases: Supplementary Results #
## This repository contains the detailed evaluation results that complement the main paper.##
## Please click the **Source** tab at the left navigation. Then you will see Supplementary folder. It has a file called **supplementary_file.pdf**. It has 9 sections and provides detailed information for the main paper.##
## Supplementary folder also has a folder called **Sliding_Windows**. It contains detailed results for sliding window experiment mentioned in the main paper.##
## Please contact karin.verspoor@unimelb.edu.au if you have any queries.##